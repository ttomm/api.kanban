const chai = require('chai');
const should = chai.should();
const sinon = require('sinon');
const User = require('../models/user');

describe('User', () => {
    it('should be invalid without "name" property', (done) => {
        const user = new User();

        user.validate((err) => {
            err.errors.name.should.exist;
            done();
        });
    });


    it('should have be invalid with "password" property which does not contain at least one special sign [@#$%&*]', (done) => {
        const user = new User({ password: 'asdqwe' });

        user.validate((err) => {
            err.errors.password.should.exist;
            done();
        });
    });


    // it('"authenticate" method should take 2 params', sinon.test(function() {
    //     this.stub(User, 'findOne');
    //     const expectedName = 'admin';
    //     const expectedPass = '*as#qw?';
    //
    //     const user = new User({ name: expectedName, password: expectedPass });
    //     user.authenticate(function() {});
    //
    //     sinon.assert.calledWith(User.findOne, {
    //         name: expectedName,
    //         password: expectedPass
    //     });
    // }))
});