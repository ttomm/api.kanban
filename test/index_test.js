const request = require('superagent');
const chai = require('chai');
const should = chai.should();
const sinon = require('sinon');
const app = require('../index');
const User = require('../models/user');

describe('GET method to /api/user', () => {
    it ('should return HTTP status code 200', (done) => {
        request
            .get('http://localhost:10101/api/user')
            .end((err, res) => {
                res.status.should.be.equal(200);
                res.body.should.be.an('array');
                res.body.length.should.be.eql(2);
                done();
            });
    });

    it ('should call "findOne" method once', sinon.test((done) => {
        const stub = sinon.stub(User, 'findOne');
        stub.yields();

        request
            .get('http://localhost:10101/api/user')
            .end((err, res) => {
                stub.restore();
                sinon.assert.calledOnce(stub);
            });
    }));
});


describe('GET method to /api/user/tomm', () => {
    it('should return an object with property "name" and value "tomm"', (done) => {
        request
            .get('http://localhost:10101/api/user/tomm')
            .end((err, res) => {
                res.status.should.be.equal(200);
                res.body.should.be.an('object');
                res.body.should.have.property('name').to.be.equal('tomm');
                done();
            });
    });
});