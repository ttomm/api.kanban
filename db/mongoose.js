const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/kanban');

const db = mongoose.connection;

db.on('error', err => console.error(`Connection error: ${err.message}`));

db.once('open', () => console.info('Connected to DB'));

module.exports = mongoose;