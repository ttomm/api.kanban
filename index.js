const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

app.get('/api/user', (req, res, next) => {
    res.status(200).json([
        {name: 'Tom', age: 30},
        {name: 'Adam', age: 35}
    ])
});

app.get('/api/user/:username', (req, res, next) => {
    res.status(200).json({name: req.params.username});
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json(err);
});

app.listen(10101, () => console.log('express... api.kanban is running'));